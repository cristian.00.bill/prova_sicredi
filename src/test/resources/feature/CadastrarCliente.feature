# language: pt

Funcionalidade: Realizar validações de Cadastro de cliente
  Com acesso ao link da aplicação
  Quero realizar cadastro e delete de um cliente

  Contexto:
    Dado que esteja na tela de listagem de cliente com o tema "Bootstrap V4 Theme" selecionado
    E acesse a tela de cadastro de novo cliente

  Cenário: Testes de cadastro de Cliente
    E insira as informações de nome "Teste Sicredi"
    E último nome do contato "Teste"
    E o primeiro nome do contato "Cristian"
    E telefone "51 9999-9999"
    E o endereço linha 1 "Av Assis Brasil, 3970"
    E o endereço linha 2 "Torre D"
    E a cidade "Porto Alegre"
    E o estado "RS"
    E o código postal "91000-000"
    E o país "Brasil"
    E o numero do empregado "2022"
    E o limite de credito "200"
    Quando clicar em salvar
    Então será exibido a mensagem de sucesso: "Your data has been successfully stored into the database. Edit Record or Go back to list"