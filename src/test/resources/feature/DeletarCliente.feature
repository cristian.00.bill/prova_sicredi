# language: pt
Funcionalidade: Deletar um cliente cadastrado
  Com acesso ao link da aplicação
  quero realizar cadastro e posteriormente a exclusão deste cliente

  Contexto:
    Dado que com o tema "Bootstrap V4 Theme" selecionado eu realize o acesso a tela de inclusão de novo cliente
    E insira as informações do formulario
    E salve esse cadastro
    E validar a mensagem "Your data has been successfully stored into the database. Edit Record or Go back to list"

  Cenário: Realizar deleçao do cliente cadastrado
    E retornar para a tela de listagem
    E realizar a busca pelo nome do cliente cadastrado
    E solicitar a exclusão deste registro
    Quando confirmar a exclusão
    Entao será exibido a mensagem "Your data has been successfully deleted from the database." confirmando a exclusao