package steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import pages.ClientePage;

public class CadastrarClienteSteps {
    public ClientePage paginaDeCliente;

    @Dado("que esteja na tela de listagem de cliente com o tema {string} selecionado")
    public void que_esteja_na_tela_de_listagem_de_cliente_com_o_tema_selecionado(String string) {
        this.paginaDeCliente = new ClientePage();
        this.paginaDeCliente.selecionarTema(string);
    }
    @E("acesse a tela de cadastro de novo cliente")
    public void acesse_a_tela_de_cadastro_de_novo_cliente() {
        this.paginaDeCliente.acessarTelaDeCadastro();
    }
    @E("insira as informações de nome {string}")
    public void insira_as_informações_de_nome(String nome) {
        paginaDeCliente.inserirNome(nome);
    }
    @E("último nome do contato {string}")
    public void último_nome_do_contato(String ultimoNomeContato) {
        this.paginaDeCliente.inserirUltimoNomeDoContato(ultimoNomeContato);
    }
    @E("o primeiro nome do contato {string}")
    public void o_primeiro_nome_do_contato(String primeiroNomeContato) {
        this.paginaDeCliente.inserirPrimeiroNomeDoContato(primeiroNomeContato);
    }
    @E("telefone {string}")
    public void telefone(String telefone) {
        this.paginaDeCliente.inserirTelefone(telefone);
    }
    @E("o endereço linha 1 {string}")
    public void o_endereço_linha_1(String endereco1) {
        this.paginaDeCliente.inserirEnderecoLinha1(endereco1);
    }
    @E("o endereço linha 2 {string}")
    public void o_endereço_linha_2(String endereco2) {
        this.paginaDeCliente.inserirEnderecoLinha2(endereco2);
    }
    @E("a cidade {string}")
    public void a_cidade(String cidade) {
        this.paginaDeCliente.inserirCidade(cidade);
    }
    @E("o estado {string}")
    public void o_estado(String estado) {
        this.paginaDeCliente.inserirEstado(estado);
    }
    @E("o código postal {string}")
    public void o_código_postal(String codigoPostal) {
        this.paginaDeCliente.inserirCodigoPostal(codigoPostal);
    }
    @E("o país {string}")
    public void o_país(String pais) {
        this.paginaDeCliente.inserirPais(pais);
    }
    @E("o numero do empregado {string}")
    public void o_numero_do_empregado(String numeroEmpregado) {
        this.paginaDeCliente.inserirNumeroEmpregado(numeroEmpregado);
    }
    @E("o limite de credito {string}")
    public void o_limite_de_credito(String limite) {
        this.paginaDeCliente.inserirLimiteDeCredito(limite);
    }
    @Quando("clicar em salvar")
    public void clicar_em_salvar() {
        this.paginaDeCliente.salvarCadastro();
    }
    @Então("será exibido a mensagem de sucesso: {string}")
    public void será_exibido_a_mensagem_de_sucesso(String mensagemSucesso) {
        this.paginaDeCliente.validarMensagemSucesso(mensagemSucesso);
        this.paginaDeCliente.fecharPagina();
    }
}
