package steps;

import com.github.javafaker.Faker;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pages.ClientePage;

public class DeletarClienteSteps {
    public ClientePage paginaDeCliente;

    @Dado("que com o tema {string} selecionado eu realize o acesso a tela de inclusão de novo cliente")
    public void que_com_o_tema_selecionado_eu_realize_o_acesso_a_tela_de_inclusão_de_novo_cliente(String tema) {
        this.paginaDeCliente = new ClientePage();
        this.paginaDeCliente.selecionarTema(tema);
        this.paginaDeCliente.acessarTelaDeCadastro();
    }
    @E("insira as informações do formulario")
    public void insira_as_informações_do_formulario() {
        this.paginaDeCliente.inserirNome(faker.name().firstName());
        this.paginaDeCliente.inserirUltimoNomeDoContato(faker.name().lastName());
        this.paginaDeCliente.inserirPrimeiroNomeDoContato(faker.name().firstName());
        this.paginaDeCliente.inserirTelefone(faker.phoneNumber().cellPhone());
        this.paginaDeCliente.inserirEnderecoLinha1(faker.address().fullAddress());
        this.paginaDeCliente.inserirEnderecoLinha2(faker.address().fullAddress());
        this.paginaDeCliente.inserirCidade(faker.address().city());
        this.paginaDeCliente.inserirEstado(faker.address().state());
        this.paginaDeCliente.inserirCodigoPostal(faker.address().zipCode());
        this.paginaDeCliente.inserirPais(faker.address().country());
        this.paginaDeCliente.inserirNumeroEmpregado(faker.number().digit());
        this.paginaDeCliente.inserirLimiteDeCredito(faker.number().digit());
    }
    @E("salve esse cadastro")
    public void salve_esse_cadastro() {
        this.paginaDeCliente.salvarCadastro();
    }
    @E("validar a mensagem {string}")
    public void validar_a_mensagem(String string) {
        this.paginaDeCliente.validarMensagemSucesso(string);
    }
    @E("retornar para a tela de listagem")
    public void retornar_para_a_tela_de_listagem() {
        this.paginaDeCliente.retornarParaListagem();
    }
    @E("realizar a busca pelo nome do cliente cadastrado")
    public void realizar_a_busca_pelo_nome_do_cliente_cadastrado() {
        this.paginaDeCliente.buscarClienteNaListagem();
    }
    @E("solicitar a exclusão deste registro")
    public void solicitar_a_exclusão_deste_registro() {
        this.paginaDeCliente.solicitarExclusaoDoCLiente();
    }
    @Quando("confirmar a exclusão")
    public void confirmar_a_exclusão() {
        this.paginaDeCliente.confirmarExclusaoDoCliente();
    }
    @Entao("será exibido a mensagem {string} confirmando a exclusao")
    public void ao_será_exibido_a_mensagem_confirmando_a_exclusao(String string) {
        this.paginaDeCliente.validarMensagemDeExclusaoDoCliente(string);
        this.paginaDeCliente.fecharPagina();
    }
}
