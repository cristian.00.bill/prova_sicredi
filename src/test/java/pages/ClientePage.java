package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class ClientePage {
    private static final String URL_PAGINA = "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap";
    private WebDriver navegador;
    static String nomeCadastrado;
    static String mensagemConfirmacao = "Are you sure that you want to delete this 1 item?";

    By acessarNovoCadastro = By.xpath("//body//div[@class='floatL t5']/a[@class='btn btn-default btn-outline-dark']");
    By clienteNome = By.id("field-customerName");
    By contatoUltimoNome = By.id("field-contactLastName");
    By contatoPrimeiroNome = By.id("field-contactFirstName");
    By telefone = By.id("field-phone");
    By enderecoLinha1 = By.id("field-addressLine1");
    By enderecoLinha2 = By.id("field-addressLine2");
    By cidade = By.id("field-city");
    By estado = By.id("field-state");
    By codigPostal = By.id("field-postalCode");
    By pais = By.id("field-country");
    By numeroEmpregado = By.id("field-salesRepEmployeeNumber");
    By limiteDeCredito = By.id("field-creditLimit");
    By salvarCadastro = By.id("form-button-save");
    By voltarParaListagem = By.xpath("//body//div[@id='report-success']/p/a[contains(.,'Go back to list')]");
    By pesquisarNomeCliente = By.name("customerName");
    By checkBoxCliente = By.xpath("//tbody//input[@type='checkbox']");
    By recarregarListagem = By.cssSelector("a[class='btn btn-default btn-outline-dark gc-refresh']");
    By botaoDeletar = By.cssSelector("a[title='Delete'][class='btn btn-outline-dark delete-selected-button']");
    By confirmarDelete = By.cssSelector("button[class='btn btn-danger delete-multiple-confirmation-button']");

    public ClientePage(){
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        this.navegador = new ChromeDriver();
        this.navegador.navigate().to(URL_PAGINA);
    }

    public void fecharPagina() {
        navegador.quit();
    }

    public void selecionarTema(String tema) {
        WebElement selectElement = this.navegador.findElement(By.id("switch-version-select"));
        Select selectObject = new Select(selectElement);
        selectObject.selectByVisibleText(tema);
        Assert.assertFalse(this.navegador.getCurrentUrl().equals(this.URL_PAGINA));
    }

    public void acessarTelaDeCadastro() {
        navegador.findElement(acessarNovoCadastro).click();
        Assert.assertFalse(this.navegador.getCurrentUrl().equals(this.URL_PAGINA));
    }

    public void inserirNome(String nome) {
        navegador.findElement(this.clienteNome).sendKeys(nome);
        nomeCadastrado = nome;
    }

    public void inserirUltimoNomeDoContato(String ultimoNome) {
        navegador.findElement(this.contatoUltimoNome).sendKeys(ultimoNome);
    }

    public void inserirPrimeiroNomeDoContato(String primeiroNomeContato) {
        navegador.findElement(this.contatoPrimeiroNome).sendKeys(primeiroNomeContato);
    }

    public void inserirTelefone(String telefone) {
        navegador.findElement(this.telefone).sendKeys(telefone);
    }

    public void inserirEnderecoLinha1(String endereco1) {
        navegador.findElement(this.enderecoLinha1).sendKeys(endereco1);
    }

    public void inserirEnderecoLinha2(String endereco2) {
        navegador.findElement(this.enderecoLinha2).sendKeys(endereco2);
    }

    public void inserirCidade(String cidade) {
        navegador.findElement(this.cidade).sendKeys(cidade);
    }

    public void inserirEstado(String estado) {
        navegador.findElement(this.estado).sendKeys(estado);
    }

    public void inserirCodigoPostal(String codPostal) {
        navegador.findElement(this.codigPostal).sendKeys(codPostal);
    }

    public void inserirPais(String pais) {
        navegador.findElement(this.pais).sendKeys(pais);
    }

    public void inserirNumeroEmpregado(String numeroEmpregado) {
        navegador.findElement(this.numeroEmpregado).sendKeys(numeroEmpregado);
    }

    public void inserirLimiteDeCredito(String limite) {
        navegador.findElement(limiteDeCredito).sendKeys(limite);
    }

    public void salvarCadastro() {
        navegador.findElement(salvarCadastro).click();
    }

    public void validarMensagemSucesso(String mensagemSucesso) {
        WebDriverWait wait = new WebDriverWait(navegador, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(By.id("report-success")));
        String mensagemExibida = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//body//div[@id='report-success']/p"))).getText();
        Assert.assertEquals(mensagemSucesso, mensagemExibida);
    }

    public void retornarParaListagem() {
        WebDriverWait wait = new WebDriverWait(navegador, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(By.id("report-success")));
        navegador.findElement(this.voltarParaListagem).click();
    }

    public void buscarClienteNaListagem() {
        navegador.findElement(this.pesquisarNomeCliente).sendKeys(nomeCadastrado);
        navegador.findElement(this.recarregarListagem).click();
        WebDriverWait wait = new WebDriverWait(navegador, Duration.ofSeconds(5));
        String nomeExibido = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//body//div[@class='scroll-if-required']//td[contains(.,'"+nomeCadastrado+"')]"))).getText();
        Assert.assertEquals(nomeCadastrado,nomeExibido);
    }

    public void solicitarExclusaoDoCLiente() {
        navegador.findElement(this.checkBoxCliente).click();
        navegador.findElement(this.botaoDeletar).click();
    }

    public void confirmarExclusaoDoCliente() {
        WebDriverWait wait = new WebDriverWait(navegador, Duration.ofSeconds(5));
        String mensagemExibida = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("p[class='alert-delete-multiple-one']"))).getText();
        new WebDriverWait(navegador, Duration.ofSeconds(2)).until(ExpectedConditions.elementToBeClickable(confirmarDelete));
        Assert.assertEquals(mensagemConfirmacao, mensagemExibida);
        navegador.findElement(confirmarDelete).click();
    }


    public void validarMensagemDeExclusaoDoCliente(String confirmacaoEclusao) {
        WebDriverWait wait = new WebDriverWait(navegador, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span[data-growl='message']")));
        String confirmacaoDeDelete = navegador.findElement(By.cssSelector("span[data-growl='message']")).getText();
        Assert.assertEquals(confirmacaoDeDelete, confirmacaoEclusao);
    }
}
