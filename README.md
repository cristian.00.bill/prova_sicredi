# prova_sicredi

Este projeto trata-se da prova de nivelamento da SoftDesign, onde foi realizado a implementação da automação de teste E2E com Selenium Webderiver na linguagem Java e utilizando o FrameWork Cucumber.

## Descrição

Foram utilizados dois cenários de testes, sendo o primeiro o de cadastrar um cliente e o segundo cenáro o de deletar um cliente.
Conforme citado anteriormente, foi realizado a utilização do [Framework Cucumber](https://cucumber.io/docs/guides/browser-automation/) para a implementação desta automação. Para visualizar o BDD utilizado para a automação acessar o diretório "prova_sicredi/src/test/resources/feature".

 Os principais arquivos se encontram distribuidos da seguinte forma:

```bash
prova_sicredi/src/test/resources/feature
prova_sicredi/src/java/steps
prova_sicredi/src/java/pages
```

Para o cenário de Delete de Cliente, foi realizado a utilização da biblioteca Faker para gerar dados aleatórios

## Instalação

Para a execução deste projeto, foi realizado a utilização do java na versão [Java 17.0.1 JDK](https://www.oracle.com/java/technologies/downloads/#jdk17-windows) junto com [maven](https://maven.apache.org/download.cgi).

Esta automação foi realizada somente no navegador Google Chrome, portanto foi realizado a utilização do ChromeDriver na versão [99.0.4844.17](https://chromedriver.storage.googleapis.com/index.html?path=99.0.4844.17/). O arquivo se encontra no diretório "drivers".

## Testes de execução

Os comandos abaixo devem ser executados na pasta rais do projeto:

Para realizar a intalação de todas as dependências, execute

```console
$ mvn clean install
```

Para realizar a execução dos dois cenários de testes contidos no diretório "prova_sicredi/src/test/resources/feature", execute

```console
$ mvn test
```

## Status do Projeto

Projeto finalizado.

